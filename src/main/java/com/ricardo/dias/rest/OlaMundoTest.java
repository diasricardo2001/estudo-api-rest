package com.ricardo.dias.rest;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.*;


public class OlaMundoTest {

    /**
     * Metodos de teste são sempre publicos e sem retorno (Void)
     * Para o JUNIT só é considerado uma FALHA quando o retorno é um (AssetionError)
     * <p>
     * Falha: Eu executei so testes e oque era esperado não ocorreu, as validações não passaram
     * Erro: Eu não consegui executar O teste inteiro, não cheguei nas assetivas
     */
    @Test
    public void testOlaMundo() {
        Response response = request(Method.GET, "https://restapi.wcaquino.me/ola");
        Assert.assertTrue(response.getBody().asString().equals("Ola Mundo!"));
        Assert.assertTrue(response.statusCode() == 200);
        Assert.assertTrue("O Status Code Deve ser 201 (created)", response.statusCode() == 200);
        Assert.assertEquals(200, response.statusCode());

        ValidatableResponse validacao = response.then();
        validacao.statusCode(200);
    }

    @Test
    public void testImplementandoOutrasFormasRestAssured() {
        Response response = request(Method.GET, "https://restapi.wcaquino.me/ola");
        ValidatableResponse validacao = response.then();
        validacao.statusCode(200);

        get("https://restapi.wcaquino.me/ola").then().statusCode(200);

        /**
         * Given: Dado  =  Pré-Condições - (Corpo da Requisição)
         * When: Quando =  Ação de Fato - (GET, POST, PUT, DELETE, ETC...)
         * Then: Então  =  Assetivas - (Validações dos resultados)
         */

        given()
                .when()
                .get("https://restapi.wcaquino.me/ola")
                .then()
                .statusCode(200);
    }

}


