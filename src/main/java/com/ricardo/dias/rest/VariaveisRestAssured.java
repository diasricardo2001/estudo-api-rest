package com.ricardo.dias.rest;

import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class VariaveisRestAssured {

    /**
     *Desta forma toda classe que tiver o beforeClass como notação ira ultilizar o setup definido para executar o teste.
     */
    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = "http://restapi.wcaquino.me";
        RestAssured.port = 80;
        RestAssured.basePath = "/v2";
    }

    @Test
    public void devoUltilizarVariaveisEstaticas() {
        given()
                .log().all()
        .when()
                .get("/users")
        .then()
            .statusCode(200)
        ;
    }
}
