package com.ricardo.dias.validacoes;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class validacaoBody {

    @Test
    public void deveValidarBory() {

        //Ao realizar assetivas tente ordenalas por criticidade para que no caso de um erro ele ja pare no começo do teste.
        given()
                .when()
                .get("https://restapi.wcaquino.me/ola")
                .then()
                .statusCode(200)
                .body(is("Ola Mundo!"))
                .body(containsString("Mundo!"))
                .body(is(not(nullValue())));
    }
}
