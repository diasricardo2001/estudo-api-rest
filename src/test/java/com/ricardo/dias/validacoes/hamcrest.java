package com.ricardo.dias.validacoes;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;

public class hamcrest {

    @Test
    public void deveConhecerMatchersHamcrest() {

        //Verifica a igualdade dos dados
        Assert.assertThat("Maria", Matchers.is("Maria"));
        Assert.assertThat(128, Matchers.is(128));

        //Verifica o tipo do dado passado
        Assert.assertThat(128, Matchers.isA(Integer.class));
        Assert.assertThat(128d, Matchers.isA(Double.class));

        //Verifica se o dado é maior que outro
        Assert.assertThat(128d, Matchers.greaterThan(120d));

        //Verifica se o dado é menor que outro
        Assert.assertThat(128d, Matchers.lessThan(130d));

        List<Integer> impares = Arrays.asList(1, 2, 3, 5, 8);

        //Verifica o tamanho da lista
        Assert.assertThat(impares, Matchers.hasSize(5));

        //Verifica o conteudo da lista seguindo a ordem
        Assert.assertThat(impares, contains(1, 2, 3, 5, 8));

        //Verifica o conteudo da lista independente da ordem
        Assert.assertThat(impares, containsInAnyOrder(1, 2, 3, 5, 8));

        //Verifica se contem um elemento específico na lista
        Assert.assertThat(impares, hasItem(1));

        //Verifica se contem alguns elementos na lista
        Assert.assertThat(impares, hasItems(1, 2));

        //Matchers que podem se unir (Maria "não" é João)
        Assert.assertThat("Maria", is(not("João")));
        Assert.assertThat("Maria", not("João"));

        //Verifica as mais de uma condiçoes esperadas (Porta logica OU)
        Assert.assertThat("Maria", anyOf(is("Maria"), is("Tereza")));

        //Verifica se o dado tem todas as mais de 1 condições exigidas (Porta Logica E)
        Assert.assertThat("Joaquina", allOf(startsWith("Joa"), endsWith("ina"), containsString("qui")));

    }

}
